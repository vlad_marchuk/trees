package com.trees.recursionTestData;

import com.trees.models.AVLNode;
import com.trees.recursion.AVLTree;

import java.lang.reflect.Field;

public class AVLTreeTestData {
    public static final AVLTree originalTreeTest1 = new AVLTree();
    public static final AVLTree originalTreeTest2 = new AVLTree();
    public static final AVLTree addTreeTest1 = new AVLTree();
    public static final AVLTree addTreeTest2 = new AVLTree();
    public static final AVLTree deleteTreeTest1 = new AVLTree();
    public static final AVLTree deleteTreeTest2 = new AVLTree();
    public static final AVLTree reverseTreeTest1 = new AVLTree();
    public static final AVLTree reverseTreeTest2 = new AVLTree();

    public static final AVLNode ROOT1;
    public static final AVLNode ROOT2;
    public static final AVLNode ROOT_ADD1;
    public static final AVLNode ROOT_ADD2;
    public static final AVLNode ROOT_DELETE1;
    public static final AVLNode ROOT_DELETE2;
    public static final AVLNode ROOT_REVERSE1;
    public static final AVLNode ROOT_REVERSE2;

    static {
        AVLNode node1_100 = new AVLNode(100);
        AVLNode node1_50 = new AVLNode(50);
        AVLNode node1_150 = new AVLNode(150);
        AVLNode node1_30 = new AVLNode(30);
        AVLNode node1_125 = new AVLNode(125);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_30;
        node1_150.left =node1_125;

        ROOT2 = node1_100;
        ROOT1 = null;

        try{
            Field field1 = originalTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(originalTreeTest1, ROOT1);

            Field field1_1 = originalTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(originalTreeTest1, 0);

            Field field2 = originalTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(originalTreeTest2, ROOT2);

            Field field2_1 = addTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(originalTreeTest2, 5);
        }
        catch (Exception e) {

        }
    }

    static {
        // add
        AVLNode node1_100 = new AVLNode(100);
        AVLNode node1_50 = new AVLNode(50);
        AVLNode node1_150 = new AVLNode(150);
        AVLNode node1_30 = new AVLNode(30);
        AVLNode node1_125 = new AVLNode(125);
        AVLNode node1_115 = new AVLNode(115);

        node1_100.left = node1_50;
        node1_100.right = node1_125;
        node1_50.left = node1_30;
        node1_125.left = node1_115;
        node1_125.right = node1_150;

        ROOT_ADD2 = node1_100;

        AVLNode node1_47 = new AVLNode(47);

        ROOT_ADD1 = node1_47;

        try{
            Field field1 = addTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(addTreeTest1, ROOT_ADD1);

            Field field1_1 = addTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(addTreeTest1, 1);

            Field field2 = addTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(addTreeTest2, ROOT_ADD2);

            Field field2_1 = addTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(addTreeTest2, 6);
        }
        catch (Exception e) {

        }
    }

    static {
        // delete
        AVLNode node1_100 = new AVLNode(100);
        AVLNode node1_50 = new AVLNode(50);
        AVLNode node1_150 = new AVLNode(150);
//        AVLNode node1_30 = new AVLNode(30);
        AVLNode node1_125 = new AVLNode(125);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_150.left =node1_125;

        AVLNode node2_100 = new AVLNode(100);
        AVLNode node2_50 = new AVLNode(50);
//        AVLNode node2_150 = new AVLNode(150);
        AVLNode node2_30 = new AVLNode(30);
        AVLNode node2_125 = new AVLNode(125);

        node2_100.left = node2_50;
        node2_100.right = node2_125;
        node2_50.left = node2_30;

        ROOT_DELETE1 = node1_100;
        ROOT_DELETE2 = node2_100;

        try{
            Field field1 = deleteTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(deleteTreeTest1, ROOT_DELETE1);

            Field field1_1 = deleteTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(deleteTreeTest1, 4);

            Field field2 = deleteTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(deleteTreeTest2, ROOT_DELETE2);

            Field field2_1 = deleteTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(deleteTreeTest2, 4);
        }
        catch (Exception e) {

        }
    }

    static {
        // reverse
        AVLNode node1_100 = new AVLNode(100);
        AVLNode node1_50 = new AVLNode(50);
        AVLNode node1_150 = new AVLNode(150);
        AVLNode node1_30 = new AVLNode(30);
        AVLNode node1_125 = new AVLNode(125);

        node1_100.right = node1_50;
        node1_100.left = node1_150;
        node1_50.right = node1_30;
        node1_150.right =node1_125;

        ROOT_REVERSE2 = node1_100;
        ROOT_REVERSE1 = null;

        try{
            Field field1 = reverseTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(reverseTreeTest1, ROOT_REVERSE1);

            Field field1_1 = reverseTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(reverseTreeTest1, 0);

            Field field2 = reverseTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(reverseTreeTest2, ROOT_REVERSE2);

            Field field2_1 = reverseTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(reverseTreeTest2, 5);
        }
        catch (Exception e) {

        }
    }
}
